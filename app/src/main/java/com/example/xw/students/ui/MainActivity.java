package com.example.xw.students.ui;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xw.students.R;

import java.util.HashMap;
import java.util.Map;

import static com.example.xw.students.Config.*;

public class MainActivity extends AppCompatActivity {

    private Button btnOk;
    private Button btnReg;
    private EditText etLogin;
    private EditText etPassword;
    private Map<String, String> map;
    private int regWasOk = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        map = new HashMap<>();
        map.put("admin1234", "12345678");
        initUi();
        setListeners();
    }


    private void initUi(){
        Toolbar toolbaar = (Toolbar) findViewById(R.id.toolbar);
        toolbaar.setTitle("Students Project");
        setSupportActionBar(toolbaar);
        btnOk = (Button) findViewById(R.id.btnOk);
        btnReg = (Button) findViewById(R.id.btnRegistration);
        etLogin = (EditText) findViewById(R.id.etLogin);
        etPassword = (EditText) findViewById(R.id.etPassw);
    }

    private void setListeners(){

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String passw = etPassword.getText().toString();
                String log = etLogin.getText().toString();
                //TODO Сделать корректную проверку пароля
                if (!passw.equals("") && !log.equals("")){
                    if (map.get(log) != null && map.get(log).equals(passw)){
                        startActivity(new Intent(MainActivity.this, StubActivity.class));
                    } else {
                        Toast.makeText(MainActivity.this,
                                "Нет такого логина или пароля",
                                Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(MainActivity.this, RegistrationActivity.class),
                        regWasOk);
            }
        });


    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == regWasOk){
            if (resultCode == RESULT_OK) {
                String log = data.getStringExtra(LOGIN);
                String pas = data.getStringExtra(PASSWORD);
                map.put(log, pas);
            }
        }
    }
}
