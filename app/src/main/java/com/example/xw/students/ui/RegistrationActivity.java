package com.example.xw.students.ui;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.xw.students.R;

public class RegistrationActivity extends AppCompatActivity {

    private Button btnRegistration;
    private Button btnCancel;
    private EditText login;
    private EditText password;
    private EditText passwordConfirm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        initUi();
        setListeners();
    }

    private void initUi(){
        Toolbar toolbaar = (Toolbar) findViewById(R.id.toolbar);
        toolbaar.setTitle("Registration");
        setSupportActionBar(toolbaar);
        btnRegistration = (Button) findViewById(R.id.btnRegistration);
        btnCancel = (Button) findViewById(R.id.btnCancel);
        login = (EditText) findViewById(R.id.etLogin);
        password = (EditText) findViewById(R.id.etPassw);
        passwordConfirm = (EditText) findViewById(R.id.etPasswConfirm);
    }

    private void setListeners(){

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                setResult(RESULT_CANCELED, intent);
                finish();
            }
        });

        btnRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (password.getText().toString().length() < 6) {
                    Toast.makeText(RegistrationActivity.this,
                            "Пароль не должен быть короче 6 символов",
                            Toast.LENGTH_SHORT).show();
                    return;
                }


                String pasw = password.getText().toString();
                String paswConf = passwordConfirm.getText().toString();
                if (!pasw.equals(paswConf)) {
                    Toast.makeText(RegistrationActivity.this,
                            "Введенный пароли не совпадают",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                Toast.makeText(RegistrationActivity.this,
                        "Регистрация успешно пройдена",
                        Toast.LENGTH_SHORT).show();
                Intent intent = new Intent();
                intent.putExtra("login", login.getText().toString());
                intent.putExtra("password", password.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }

}
