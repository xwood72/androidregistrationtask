package com.example.xw.students.ui;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.example.xw.students.R;

public class StubActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stub);

        initUi();
    }

    private void initUi() {
        Toolbar toolbaar = (Toolbar) findViewById(R.id.toolbar);
        toolbaar.setTitle("Stub Activity");
        setSupportActionBar(toolbaar);
    }


}
